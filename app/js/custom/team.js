$(document).ready(function () {

  var swiper = new Swiper('.js-team-block__container', {
    navigation: {
      nextEl: '.js-team-block__btn-next',
      prevEl: '.js-team-block__btn-prev',
    },
  });
});