$(document).ready(function () {

  var swiper = new Swiper('.js-programs-lessons-slider', {
    navigation: {
      nextEl: '.js-team-block__btn-next',
      prevEl: '.js-team-block__btn-prev',
    },
  });

  const scrollEvent = function () {
    const scrolled = window.pageYOffset || document.documentElement.scrollTop;
    if (scrolled < window.innerHeight) {
      const scale = 1 + scrolled * 0.0001;
      const headerImage  = $('.programs__header__content__image');
      headerImage.css('transform', `scale(${scale})`);
    }
  };

  document.addEventListener('scroll', scrollEvent);
});
