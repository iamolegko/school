$(document).ready(function () {
  // init logo rotate

  const logoImage = $('.js-logo-image svg');
  const logoImageCircle = $('.js-logo-image-circle');
  const header = $('.js-header');
  const headerOffset = header.outerHeight();

  const scrollEvent = function () {
    const scrolled = window.pageYOffset || document.documentElement.scrollTop;
    var scrollHeight = $(document).height();
    var scrollPosition = Math.ceil($(window).height() + $(window).scrollTop());
    if (logoImage) {
      logoImage.css('transform', `rotate(${(scrolled) / 20}deg)`);
      logoImageCircle.css('transform', `rotate(-${(scrolled) / 10}deg)`);

      if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
        logoImage.css('transform', 'rotate(0deg)');
      }
    }

    if (header) {
      if (scrolled >= 1) {
        header.addClass('scroll');
      } else {
        header.removeClass('scroll');
      }
    }

  };

  scrollEvent();

  document.addEventListener('scroll', scrollEvent);

  // Button for mobile menu
  const mobButton =  $('.js-mob-menu');

  mobButton.click(function () {
    mobButton.toggleClass('active')
    mobButton.next().toggleClass('active')
  });

  // init animation
  if (window.innerWidth >= 1024) {
    new WOW().init();
  }

});
