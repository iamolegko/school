$(document).ready(function () {
    // Show text
    $('.js-testimonial-block__btn').click(function (e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.prev().hasClass('open')) {
            $this.prev().removeClass('open');
            $this.removeClass('open');
        } else {
            $this.prev().toggleClass('open');
            $this.toggleClass('open');
        }
    });

    // Show form for answer
    $('.js-comments-block__btn').click(function (e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.next().hasClass('open')) {
            $this.next().removeClass('open');
            $this.removeClass('open');
        } else {
            $this.next().toggleClass('open');
            $this.toggleClass('open');
        }
    });

});