$(document).ready(function () {
    // List-grid buttons
    $('.js-programms-list-btn').click(function () {
        $('.js-programms-grid').removeClass('active');
        $('.js-programms-grid-btn').removeClass('active');
        $('.js-programms-list').addClass('active');
        $('.js-programms-list-btn').addClass('active');
    });
    $('.js-programms-grid-btn').click(function () {
        $('.js-programms-list').removeClass('active');
        $('.js-programms-list-btn').removeClass('active');
        $('.js-programms-grid').addClass('active');
        $('.js-programms-grid-btn').addClass('active');
    });

    // Tabs
    $('body').on('click', '.js-programms-tabs li', function () {
        $('.js-programms-tabs li').removeClass('active');
        $(this).addClass('active');
        const tab_id = $(this).attr('data-tab');

        if ($('.js-programms-grid.active')) {
            $('.js-tabs-grid__tab').removeClass('active');
            $("." + tab_id).addClass('active');
        }
        if ($('.js-programms-list.active')) {
            $('.js-tabs-list__tab').removeClass('active');
            $("#" + tab_id).addClass('active');
        }
    });
});