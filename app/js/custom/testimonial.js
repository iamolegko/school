$(document).ready(function () {

    var swiper = new Swiper('.js-comment-block', {
        navigation: {
            nextEl: '.js-comment-block__btn-next ',
            prevEl: '.js-comment-block__btn-prev',
        },
    });
});