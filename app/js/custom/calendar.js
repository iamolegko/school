$(document).ready(function () {
    // calendar-button
    $('.js-calendar-btn').click(function () {
        $('.js-calendar-list__date').toggleClass('active');
        $('.js-calendar-btn').toggleClass('active');
    });

    //  tabs
    // $('.js-calendar-block__tabs li').click(function () {
    //     $('.js-calendar-block__tabs li').removeClass('active');
    //     $(this).addClass('active');

    //     const tab_id = $(this).attr('data-tab');
    //     $('.calendar-list__tab').removeClass('active');
    //     $("#" + tab_id).addClass('active');
    // });



    // List-grid buttons
    $('.js-list-btn').click(function () {
        $('.js-calendar-grid').removeClass('active');
        $('.js-grid-btn').removeClass('active');
        $('.js-calendar-list').addClass('active');
        $('.js-list-btn').addClass('active');
    });
    $('.js-grid-btn').click(function () {
        $('.js-calendar-list').removeClass('active');
        $('.js-list-btn').removeClass('active');
        $('.js-calendar-grid').addClass('active');
        $('.js-grid-btn').addClass('active');
    });

    // Tabs
    $('body').on('click', '.js-calendar-block__tabs li', function () {
        $('.js-calendar-block__tabs li').removeClass('active');
        $(this).addClass('active');
        const tab_id = $(this).attr('data-tab');
        // It does not work as it should
        if($('.js-calendar-list.active')) {
            $('.calendar-list__tab').removeClass('active');
            $("#" + tab_id).addClass('active');
        }
        if($('.js-calendar-grid.active')) {
            $('.calendar-grid__tab').removeClass('active');
            $("." + tab_id).addClass('active');
        }
    });
});