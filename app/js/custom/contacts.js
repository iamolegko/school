const image = 'images/svg/mapmarker.svg';

function initMap() {
  const cords = {lat: 50.585833, lng: 30.488505};
  const map = new google.maps.Map(document.getElementById('map'), {
    center: cords,
    zoom: 14,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  });
  const icon = {
    url: image,
    size: new google.maps.Size(50, 70),
    scaledSize: new google.maps.Size(50, 70),
  };
  const marker = new google.maps.Marker({
    position: cords,
    map,
    icon,
  });
}
