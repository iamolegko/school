$(document).ready(function () {
  // initialize head slider
  var headSlider = new Swiper ('.js-head-slider', {
    loop: true,
    speed: 1000,
    pagination: {
      el: '.js-head-slider-pagination',
    },
    autoplay: {
      disableOnInteraction: true,
      delay: 5000,
    },
  });

  // initialize  programs slider
  if ($(window).width() >= 1440) {
    var programsSlider = new Swiper('.js-programs-slider', {
      loop: true,
      slidesPerView: 4,
      autoplay: {
        disableOnInteraction: true,
        delay: 5000,
      },
    });
  } else if ($(window).width() >= 1024) {
    var programsSlider = new Swiper('.js-programs-slider', {
      loop: true,
      slidesPerView: 3,
      autoplay: {
        disableOnInteraction: true,
        delay: 5000,
      },
    });
  } else if ($(window).width() >= 768) {
    var swiper = new Swiper('.js-programs-slider', {
      slidesPerView: 2,
      slidesPerColumn: 2,
      spaceBetween: 0,
      mousewheel: true,
    });
  } else {
    var swiper = new Swiper('.js-programs-slider', {
      direction: 'vertical',
      slidesPerView: 4,
      spaceBetween: 0,
      mousewheel: true,
    });
    $('.programs-slider__slide').css("height", "360px");
  }

  // initialize  closest-events slider
  var closestEventsSlider = new Swiper ('.js-closest-events-slider', {
    loop: true,
    speed: 500,
    autoplay: {
      disableOnInteraction: true,
      delay: 5000,
    },
    navigation: {
      nextEl: '.js-closest-events-slider-next',
      prevEl: '.js-closest-events-slider-prev',
    },
  });

  //  tabs
  $('.js-events-list-tabs li').click(function () {
    $('.js-events-list-tabs li').removeClass('active');
    $(this).addClass('active');

    const tab_id = $(this).attr('data-tab');
    $('.tab-content').removeClass('active');
    $("#"+tab_id).addClass('active');
  });

  var tabsSlider = new Swiper('.js-events-list-tabs', {
    slidesPerView: 4,
    pagination: {
      clickable: true,
    },
  });

  // initialize  sliders in tabs
  $('.js-tabs-slider').each(function () {
    const eventsSlider = new Swiper (this, {
      loop: true,
      speed: 1000,
      autoplay: {
        disableOnInteraction: true,
        delay: 10000,
      },
      navigation: {
        nextEl: '.js-list-events-slider-next',
        prevEl: '.js-list-events-slider-prev',
      },
    });
  });

  // initialize  parthners sliders
  if ($(window).width() >= 1920) {
    var parthnersSlider = new Swiper('.js-parthners-slider', {
      loop: true,
      slidesPerView: 6,
      autoplay: {
        disableOnInteraction: true,
        delay: 5000,
      },
      navigation: {
        nextEl: '.js-parthners-slider-next',
        prevEl: '.js-parthners-slider-prev',
      },
    });
  } else if ($(window).width() >= 1440) {
    var parthnersSlider = new Swiper('.js-parthners-slider', {
      loop: true,
      slidesPerView: 5,
      autoplay: {
        disableOnInteraction: true,
        delay: 5000,
      },
      navigation: {
        nextEl: '.js-parthners-slider-next',
        prevEl: '.js-parthners-slider-prev',
      },
    });
  } else if ($(window).width() >= 1024) {
    var parthnersSlider = new Swiper('.js-parthners-slider', {
      loop: true,
      slidesPerView: 4,
      autoplay: {
        disableOnInteraction: true,
        delay: 5000,
      },
      navigation: {
        nextEl: '.js-parthners-slider-next',
        prevEl: '.js-parthners-slider-prev',
      },
    });
  } else if ($(window).width() >= 768) {
    var parthnersSlider = new Swiper('.js-parthners-slider', {
      loop: true,
      slidesPerView: 3,
      autoplay: {
        disableOnInteraction: true,
        delay: 5000,
      },
      navigation: {
        nextEl: '.js-parthners-slider-next',
        prevEl: '.js-parthners-slider-prev',
      },
    });
  } else {
    var parthnersSlider = new Swiper('.js-parthners-slider', {
      loop: true,
      slidesPerView: 2,
      autoplay: {
        disableOnInteraction: true,
        delay: 5000,
      },
      navigation: {
        nextEl: '.js-parthners-slider-next',
        prevEl: '.js-parthners-slider-prev',
      },
    });
  }

  // Video
  $(".js-video-about-school__video-play").click(function () {
    $('.js-video-about-school__poster').css("display", "none");
    // TODO: Play video
    // $('.js-video-about-school__video').playVideo();
  });


  const scrollEvent = function () {
    const scrolled = window.pageYOffset || document.documentElement.scrollTop;
    if (scrolled < window.innerHeight) {
      const scale = 1 + scrolled * 0.0001;
      const headerImage  = $('.main-header__content__image');
      headerImage.css('transform', `scale(${scale})`);
    }
  };

  document.addEventListener('scroll', scrollEvent);

});
