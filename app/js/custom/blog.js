function customSelect(el) {
    var options = [],
        option = $(el).children('option'),
        customSelect;

    $(el).hide();

    // Create custom select
    $(option).each(function () {
        options.push($(this).html());
    });

    $(el).after('<ul class="select-tag__select" data-selected-value="' + options[0] + '">');
    customSelect = $(el).siblings('.select-tag__select');
    $(customSelect).append('<li class="select-tag__option"><span>' + options[0] + '</span>');
    $(customSelect).children('.select-tag__option').append('<ul class="select-tag__options">');

    for (var i = 1; i < options.length; i++) {
        $(customSelect).find('.select-tag__options').append('<li data-value=' + options[i] + '>' + options[i] + '</li>');
    }

    // Click action & synchronization with origin select for submitting form     
    $(customSelect).click(function () {
        $(this).toggleClass('active');
        $('.select-tag__options', this).toggleClass('active');
    });

    $(customSelect).find('.select-tag__options li').click(function () {
        var selection = $(this).text();
        var dataValue = $(this).attr('data-value');
        var selected = $(customSelect).find('.select-tag__option span').text(selection);
        for (var i = 1; i < option.length; i++) {
            if ($(option[i]).text() === selected.text()) {
                $(option[i]).attr('selected', 'true');
                $(option[i]).siblings().removeAttr('selected');
            }
        };

        $(customSelect).attr('data-selected-value', dataValue);
    });
};


customSelect('#select-tags');
